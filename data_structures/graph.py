import sys

class Graph:
    def __init__(self, data, matrix) -> None:
        self.data = data
        self.matrix = matrix

    def print_sagittal_diagram(self):
        matrix_size = len(self.matrix)

        for i in range(matrix_size):          
            has_sucessor = False
            for j in range(matrix_size):
                if self.matrix[i][j]:               
                    has_sucessor = True                   
                    print(f'{self.data[i]} -> {self.data[j]}')

            if not has_sucessor:
                print(self.data[i])

        print()

    def borders(self, nodes):
        # The case where you provide only 1 node.
        # Create a list containing 1 element.
        if type(nodes) is int:
            nodes = (nodes,)

        matrix_size = len(self.matrix)
        border_nodes = []

        for i in range(len(nodes)):
            node_index = self.data.index(nodes[i])
            
            for j in range(matrix_size):
                edge = self.matrix[node_index][j]
                node = self.data[j]

                if edge and not node in nodes and not node in border_nodes:
                    border_nodes.append(node)

        return border_nodes

    def is_generic_browse(self, L):        
        for i in range(1, len(L)):
            borders = self.borders(L[:i])

            if L[i] not in borders:
                return False

        return True

    def predecessors(self, node, valid_nodes = None):
        # If 1 -> 1, 1 is a predecessor of 1?
        predecessors = []
        matrix_size = len(self.matrix)

        node_index = self.data.index(node)
        
        for i in range(matrix_size):
            if node_index != i and self.matrix[i][node_index]:
                if valid_nodes != None:
                    if self.data[i] in valid_nodes:
                        predecessors.append(self.data[i])
                else:
                    predecessors.append(self.data[i])

        return predecessors

    def regeneration_points(self, browse):
        if browse == None:
            return []
        elif type(browse) is int:
            return [browse]
        elif len(browse) == 0:
            return []
        elif len(browse) == 1:
            return browse

        browse_size = len(browse)    
        regeneration_points = [browse[0]]
        
        for i in range(1, browse_size):
            borders = self.borders(browse[:i])

            if len(borders) == 0:
                regeneration_points.append(browse[i])

        return regeneration_points

    def choice_graph(self, browse):
        covering_forest_size = len(browse)
        regeneration_points = self.regeneration_points(browse)
        sorted_browse = sorted(browse)
        covering_forest = []

        for i in range(covering_forest_size):
            covering_forest.append([])
            for j in range(covering_forest_size):
                covering_forest[i].append(0)

        for i in range(len(browse)):            
            node = browse[i]

            if node in regeneration_points:
                continue
                        
            predecessors = self.predecessors(node, browse[:i])

            if len(predecessors) == 0:
                continue

            predecessor = predecessors[0]    
            predecessor_index = sorted_browse.index(predecessor)        

            covering_forest[predecessor_index][i] = 1 

        return Graph(sorted_browse, covering_forest)

    def is_valid_covering_forest(self, matrix):
        pass

    # A utility function to find the vertex with  
    # minimum distance value, from the set of vertices  
    # not yet included in shortest path tree 
    def minDistance(self, dist, sptSet): 
  
        # Initilaize minimum distance for next node 
        min = sys.maxsize
  
        # Search not nearest vertex not in the  
        # shortest path tree 
        for v in range(len(self.data)): 
            if dist[v] < min and sptSet[v] == False: 
                min = dist[v] 
                min_index = v 
  
        return min_index 

    # Funtion that implements Dijkstra's single source  
    # shortest path algorithm for a graph represented  
    # using adjacency matrix representation 
    def dijkstra(self, src): 
        number_of_vertices = len(self.data)
        dist = [sys.maxsize] * number_of_vertices
        dist[src] = 0
        sptSet = [False] * number_of_vertices

        for cout in range(number_of_vertices): 
            # Pick the minimum distance vertex from  
            # the set of vertices not yet processed.  
            # u is always equal to src in first iteration 
            u = self.minDistance(dist, sptSet) 
  
            # Put the minimum distance vertex in the  
            # shotest path tree 
            sptSet[u] = True
  
            # Update dist value of the adjacent vertices  
            # of the picked vertex only if the current  
            # distance is greater than new distance and 
            # the vertex in not in the shotest path tree 
            for v in range(number_of_vertices): 
                if self.matrix[u][v] > 0 and sptSet[v] == False and dist[v] > dist[u] + self.matrix[u][v]: 
                    dist[v] = dist[u] + self.matrix[u][v] 
  
        dictionnary_distances = { }

        for i in range(len(dist)):
            dictionnary_distances[self.data[i]] = dist[i]

        return dictionnary_distances