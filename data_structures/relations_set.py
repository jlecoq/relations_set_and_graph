from __future__ import annotations
from copy import deepcopy
from data_structures.relations.is_partial_order import is_partial_order
from math_utils.operators import *

class RelationsSet:
    def __init__(self, data: list, operator: Operator) -> None:
        self.data = data
        self.operator = operator
        self.operator_func = operator.func
        self.relation_symbol = operator.symbol
        self.matrix = self.sagittal_diagram_matrix()

    def __str__(self):
        str_representation = '({'

        for index, element in enumerate(self.data):
            if index == len(self.data) - 1:
                str_representation += str(element)
            else:
                str_representation += str(element) + ','

        str_representation += '},' + self.relation_symbol + ')'

        return str_representation

    def sagittal_diagram_matrix(self):
        matrix_size = len(self.data)
        relation_matrix = [[0] * matrix_size for x in range(matrix_size)]

        for i in range(matrix_size):
            for j in range(matrix_size):
                relation_matrix[i][j] = 1 if self.operator_func(
                    self.data[i], self.data[j]) else 0

        return relation_matrix

    def hasse_diagram_matrix(self):
        diagram = deepcopy(self.matrix)
        matrix_size = len(diagram)

        for i in range(matrix_size):
            for j in range(matrix_size):
                # If it is a link on myself.
                if i == j:
                    diagram[i][j] = 0
                    continue

                # If we are in the lower left part of the matrix.
                if j < i:
                    continue

                # I have a link (i, j).
                if diagram[i][j] == 1:
                    # Loop through the list of my possible links.
                    for k in range(matrix_size):
                        # For each of my link.
                        if diagram[i][k] == 1 and j != k:
                            # If this link has a link with j: (k, j).
                            if diagram[k][j] == 1:
                                diagram[i][j] = 0
                                break

        return diagram

    def print_sagittal_diagram(self):
        matrix_size = len(self.matrix)

        for i in range(matrix_size):          
            has_sucessor = False
            for j in range(matrix_size):
                if self.matrix[i][j]:               
                    has_sucessor = True                   
                    print(f'{self.data[i]} -> {self.data[j]}')

            if not has_sucessor:
                print(self.data[i])

        print()

    def print_hasse_diagram(self):
        relation_matrix = self.hasse_diagram_matrix()
        matrix_size = len(relation_matrix)

        for i in range(matrix_size):
            for j in range(matrix_size):
                if relation_matrix[i][j] == 1:
                    print(f'{self.data[i]} -> {self.data[j]}')

        print()

    def lower_bounds(self, subset: RelationsSet):
        lower_bounds = []

        for x_element in self.data:
            is_lower_bound = True

            for p_element in subset.data:
                if not self.operator_func(x_element, p_element):
                    is_lower_bound = False
                    break

            if is_lower_bound:
                lower_bounds.append(x_element)

        return lower_bounds

    def upper_bounds(self, subset: RelationsSet):
        upper_bounds = []
        
        for x_element in self.data:
            is_upper_bound = True

            for p_element in subset.data:                
                if not self.operator_func(p_element, x_element):                    
                    is_upper_bound = False
                    break

            if is_upper_bound:
                upper_bounds.append(x_element)

        return upper_bounds

    def supremum(self, subset: RelationsSet):
        up_bounds = self.upper_bounds(subset)

        for probable_least_upper_bound in up_bounds:
            is_least_upper_bound = True

            for up_bound in up_bounds:
                if not self.operator_func(probable_least_upper_bound, up_bound):
                    is_least_upper_bound = False
                    break

            if is_least_upper_bound:
                return probable_least_upper_bound

        return None

    def infimum(self, subset: RelationsSet):
        low_bounds = self.lower_bounds(subset)

        for probable_greatest_lower_bound in low_bounds:
            is_greatest_lower_bound = True

            for low_bound in low_bounds:
                if not self.operator_func(low_bound, probable_greatest_lower_bound):
                    is_greatest_lower_bound = False
                    break

            if is_greatest_lower_bound:
                return probable_greatest_lower_bound

        return None

    def maximal_elements(self):
        maximal_elements = []

        for probable_maximal_element in self.data:
            is_maximal_element = True

            for element in self.data:
                if probable_maximal_element != element and self.operator_func(probable_maximal_element, element):
                    is_maximal_element = False
                    break

            if is_maximal_element:
                maximal_elements.append(probable_maximal_element)

        return maximal_elements

    def minimal_elements(self):
        minimal_elements = []

        for probable_maximal_element in self.data:
            is_minimal_element = True

            for element in self.data:
                if probable_maximal_element != element and self.operator_func(element, probable_maximal_element):
                    is_minimal_element = False
                    break

            if is_minimal_element:
                minimal_elements.append(probable_maximal_element)

        return minimal_elements

    def least_element(self):
        for probable_least_element in self.data:
            is_least_element = True

            for element in self.data:
                if not self.operator_func(probable_least_element, element):
                    is_least_element = False
                    break

            if is_least_element:
                return probable_least_element

        return None

    def greatest_element(self):
        for probable_greatest_element in self.data:
            is_greatest_element = True

            for element in self.data:
                if not self.operator_func(element, probable_greatest_element):
                    is_greatest_element = False
                    break

            if is_greatest_element:
                return probable_greatest_element

        return None

    def is_lattice(self):
        """
            Trellis is the french word for lattice.
        """

        if not is_partial_order(self.matrix):
            return False
        
        for i in range(len(self.data)):
            for j in range(len(self.data)):
                if i == j:
                    continue                            

                sub_set = RelationsSet([self.data[i], self.data[j]], self.operator)

                supremum = self.supremum(sub_set)
                infimum = self.infimum(sub_set)
                
                if (supremum == None) or (infimum == None):
                    return False
                
        return True

    def is_lattice_algebric(self, set: RelationsSet):
        """
            Not implemented.
        """

        pass

    def universal_element(self):
        return self.greatest_element()

    def null_element(self):
        return self.least_element()