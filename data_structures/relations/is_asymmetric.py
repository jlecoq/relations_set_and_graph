def is_asymmetric(matrix):
    """
        ∀x ∈ X ∧ ∀y ∈ X, if xRy then ¬yRx. 
        A relation is asymmetric if and only if it is both antisymmetric and irreflexive.
    """

    matrix_len = len(matrix)

    for i in range(matrix_len):
        for j in range(matrix_len): 
            if matrix[i][j]:
                if matrix[j][i]:
                    return False

    return True