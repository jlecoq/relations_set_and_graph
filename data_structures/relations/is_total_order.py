from data_structures.relations.is_transitive import is_transitive
from data_structures.relations.is_connex import is_connex
from data_structures.relations.is_antisymmetric import is_antisymmetric

def is_total_order(relation_matrix):
    """
        The connexity of a partial order implies the reflexivity.
        The connexity is also called the comparability.
    """

    return is_antisymmetric(relation_matrix) and is_transitive(relation_matrix) and is_connex(relation_matrix)