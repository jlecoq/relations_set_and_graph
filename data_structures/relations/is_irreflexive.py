def is_irreflexive(matrix):
    """
        ∀x ∈ X, ¬xRx∀x ∈ X, ¬xRx
    """
    
    matrix_len = len(matrix)

    for i in range(matrix_len):
        if matrix[i][i]:
            return False
    
    return True