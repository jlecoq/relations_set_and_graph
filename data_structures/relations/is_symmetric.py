def is_symmetric(matrix):
    """
        ∀x ∈ X ∧ ∀y ∈ X, if xRy then yRx. 
    """

    matrix_size = len(matrix)

    for i in range(matrix_size):
        for j in range(matrix_size):
            if matrix[i][j]:
                if not matrix[j][i]:
                    return False

    return True