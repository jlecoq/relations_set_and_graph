from data_structures.relations.is_reflexive import is_reflexive
from data_structures.relations.is_symmetric import is_symmetric
from data_structures.relations.is_transitive import is_transitive

def is_equivalent(matrix):
    return is_reflexive(matrix) and is_symmetric(matrix) and is_transitive(matrix)
    