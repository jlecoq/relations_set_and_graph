def is_intransitive(matrix):
    matrix_size = len(matrix)

    for x in range(matrix_size):
        for y in range(matrix_size): 
            # I have a link (x, y).         
            if matrix[x][y]:

                # I check if I have a link (y, z).
                # If yes, I check if I have a link (x, z)
                # If yes, the relation is not antitransitive.
                for z in range(0, matrix_size):
                    if matrix[y][z] and matrix[x][z]:
                        return False

    return True