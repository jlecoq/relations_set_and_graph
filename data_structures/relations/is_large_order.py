from data_structures.relations.is_reflexive import is_reflexive
from data_structures.relations.is_antitransitive import is_antitransitive
from data_structures.relations.is_transitive import is_transitive

def is_large_order(matrix):
    return is_reflexive(matrix) and is_antisymmetric(matrix) and is_transitive(matrix)