from data_structures.relations.is_strict_order import is_strict_order
from data_structures.relations.is_connex import is_connex

def is_strict_total_order(relation_matrix):
    return is_strict_order(relation_matrix) and is_connex(relation_matrix)