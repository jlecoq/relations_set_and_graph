def is_transitive(relation_matrix):
    """
        Returns true if a relation is transitive, false otherwise.
        ∀x ∈ X ∧ ∀y ∈ X ∧ ∀z ∈ X, if xRy ∧ yRz then xRz
    """

    matrix_size = len(relation_matrix)

    for x in range(0, matrix_size):
        for y in range(0, matrix_size):

            # I have a link (x, y).
            if relation_matrix[x][y] == 1:

                # I check if I have a link (y, z).
                # If yes, I check if I have a link (x, z)
                # If no, the relation is not transitive.
                for z in range(0, matrix_size):
                    if relation_matrix[y][z] == 1 and relation_matrix[x][z] == 0:
                        return False

    return True


def transitive_relations(transitive_matrix):
    """
        Return the list of relations of a given transitive matrix.
    """

    all_relations = []
    relations = []

    matrix_size = len(transitive_matrix)

    for x in range(0, matrix_size):
        for y in range(0, matrix_size):

            # I have a link (x, y).
            if transitive_matrix[x][y] == 1:
                relations.append((x, y))

                # I check if I have a link (y, z).
                # If yes, I check if I have a link (x, z)
                # If no, the relation is not transitive.
                for z in range(0, matrix_size):
                    if transitive_matrix[y][z] == 1 and transitive_matrix[x][z] == 1:
                        relations.append((y, z))
                        relations.append((x, z))
                        all_relations.append(relations.copy())
                        relations = relations[:1]

                relations.clear()

    return all_relations


def print_relations_of_transitive_matrix(transitive_relations, name):
    print(f'The list of transitive relations in the {name}:')

    for relations in transitive_relations:
        relations_str = ''

        for index, relation in enumerate(relations):
            if index < 2:
                relations_str += str(relation) + ', '
            else:
                relations_str += str(relation)

        print(relations_str)