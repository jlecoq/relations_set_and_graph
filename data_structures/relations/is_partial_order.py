from data_structures.relations.is_reflexive import is_reflexive
from data_structures.relations.is_antisymmetric import is_antisymmetric
from data_structures.relations.is_transitive import is_transitive

def is_partial_order(relation_matrix):
    return is_reflexive(relation_matrix) and is_antisymmetric(relation_matrix) and is_transitive(relation_matrix)