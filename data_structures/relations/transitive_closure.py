from copy import deepcopy

def transitive_closure(matrix):
    """
        Returns the transitive closure of a graph (stored as a matrix).
    """

    # Initialize the transitive closure matrix same as the input graph matrix.
    transitiveClosure = deepcopy(matrix)

    # Matrix size (number of vertices in the graph).
    matrix_size = len(matrix)

    '''Add all vertices one by one to the set of intermediate
        vertices.
         ---> Before start of a iteration, we have reachability value
         for all pairs of vertices such that the reachability values
          consider only the vertices in set
        {0, 1, 2, .. k-1} as intermediate vertices.
          ----> After the end of an iteration, vertex no. k is
         added to the set of intermediate vertices and the
        set becomes {0, 1, 2, .. k}'''
    for k in range(matrix_size):
        for i in range(matrix_size):  # Pick all vertices as source one by one.
            for j in range(matrix_size): # Pick all vertices as destination for the above picked source.

                # If vertex k is on a path from i to j,
                # then make sure that the value of transitiveClosure[i][j] is 1
                transitiveClosure[i][j] = transitiveClosure[i][j] or (transitiveClosure[i][k] and transitiveClosure[k][j])

    return transitiveClosure