from data_structures.relations.is_irreflexive import is_irreflexive
from data_structures.relations.is_transitive import is_transitive

def is_strict_order(relation_matrix):
    """
        The irreflexivity and the transitivity implies the asymmetry
    """

    return is_irreflexive(relation_matrix) and is_transitive(relation_matrix)