def is_antisymmetric(matrix):
    """
        ∀x ∈ X ∧ ∀y ∈ X, if xRy ∧ yRx then x = y
    """

    matrix_len = len(matrix)

    for i in range(matrix_len): 
        for j in range(matrix_len): 
            if matrix[i][j] and i != j:
                if matrix[j][i]:
                    return False

    return True