def is_connex(matrix):
    """
        Also known as totality
        ∀x ∈ X ∧ ∀y ∈ X, xRy or yRx
    """

    matrix_len = len(matrix)

    for i in range(matrix_len): 
        for j in range(matrix_len):
            if not matrix[i][j] and not matrix[j][i]:
                return False
            
    return True