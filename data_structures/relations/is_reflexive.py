def is_reflexive(matrix):
    """
        ∀x ∈ X, xRx
    """

    len_matrix = len(matrix)

    for i in range(len_matrix):
        if not matrix[i][i]:
            return False

    return True