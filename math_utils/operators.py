class Operator:
    def __init__(self, func, symbol) -> None:
        self.func = func
        self.symbol = symbol
        
divide_func = lambda num1, num2: True if num2 % num1 == 0 else False
divide_operator = Operator(divide_func, '|')

greater_than_or_equal_to_func = lambda num1, num2: num1 >= num2
greater_than_or_equal_to_operator = Operator(greater_than_or_equal_to_func, '>=')

greater_func = lambda num1, num2: num1 > num2
greater_operator = Operator(greater_func, '>')

smaller_func = lambda num1, num2: num1 < num2
smaller_operator = Operator(smaller_func, '<')

smaller_than_or_equal_to_func = lambda num1, num2: num1 <= num2
smaller_than_or_equal_to_operator = Operator(smaller_than_or_equal_to_func, '<=')