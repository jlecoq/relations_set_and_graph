from numpy import matrix
from data_structures.graph import Graph
from utils.print_color import OKGREEN, ENDC

def test_graph():
    """
        Test retrieved from the teacher's example
    """

    print(f'{OKGREEN}RUN TEST: graph.{ENDC}\n')

    graph_matrix = [
        [0, 1, 1, 0, 0, 0, 0, 0],
        [1, 0, 0, 1, 1, 0, 0, 0],
        [1, 0, 0, 1, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 0, 0, 0],
        [0, 1, 0, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 0]
    ]

    vertices = (1, 2, 3, 4, 5, 6, 7, 8)

    graph = Graph(vertices, graph_matrix)

    print(f'Vertices of the graph G: {vertices}\n')

    print('Matrix of the graph G:')
    print(matrix(graph.matrix))

    print(f'\nSagittal diagram of the graph G:')
    graph.print_sagittal_diagram()

    vertices = (3, 5)
    borders = graph.borders(vertices)
    print(f'The borders of {vertices} are: {borders}')

    vertices = (1, 4)
    borders = graph.borders(vertices)
    print(f'The borders of {vertices} are: {borders}\n')

    L1 = (1, 3, 4, 5, 7, 8, 6)
    is_generic = graph.is_generic_browse(L1)    
    print(f'The browse L1 {L1} is generic: {is_generic}')

    L2 = (5, 6, 2, 7, 8, 1, 3, 4)
    is_generic = graph.is_generic_browse(L2)    
    print(f'The browse L2 {L2} is generic: {is_generic}')

    L3 = (5, 6, 3, 2, 1, 4, 8, 7)
    is_generic = graph.is_generic_browse(L3)    
    print(f'The browse L3 {L3} is generic: {is_generic}')

    regeneration_points = graph.regeneration_points(L2)
    print(f'\nThe regeneration points of L2 are: {regeneration_points}')
    
    regeneration_points = graph.regeneration_points(L3)
    print(f'The regeneration points of L3 are: {regeneration_points}')

    predecessors = graph.predecessors(4)
    print(f'\nThe predecessors of 4 are: {predecessors}')

    predecessors = graph.predecessors(6)
    print(f'The predecessors of 6 are: {predecessors}\n')

    choice_graph = graph.choice_graph(L3)
    print('Matrix of a choice graph of L3:')
    print(matrix(choice_graph.matrix))

    print('\nSagittal diagram of a choice graph of L3:')
    choice_graph.print_sagittal_diagram()

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

    # graph_matrix_prime = [
        # [0, 1, 1, 0, 0, 0, 0, 0],
        # [0, 0, 0, 1, 1, 0, 0, 0],
        # [0, 0, 0, 0, 0, 0, 0, 0],
        # [0, 0, 1, 0, 1, 0, 0, 0],
        # [0, 0, 0, 0, 0, 1, 1, 0],
        # [0, 0, 0, 0, 0, 0, 0, 0],
        # [0, 0, 0, 0, 0, 1, 0, 0],
        # [0, 0, 0, 0, 0, 0, 1, 0]
    # ]
# 
    # graph_prime = Graph(values, graph_matrix_prime)
    # print(f'Graph G\': ')
    # graph_prime.print_sagittal_diagram()

    # nodes = (6, 9)
    # borders = graph.borders(nodes)
    # print(borders)

    # L4 = [1, 2, 3, 4, 5, 6, 7, 8]
    # regeneration_points = graph_prime.regeneration_points(L4)
    # print(f'The regeneration points of L4 are: {regeneration_points}')

    # predecessors = graph_prime.predecessors(4)
    # print(f'\nPredecessors of 7: {predecessors}')

    # predecessors = graph_prime.predecessors(7)
    # print(f'Predecessors of 7: {predecessors}')

    # predecessors = graph_prime.predecessors(6)
    # print(f'Predecessors of 6: {predecessors}')

    # covering_forest2 = graph_prime.covering_forest(L4)
    # print('Covering forest:')

    # covering_forest2.print_sagittal_diagram()
    # print('Matrix of the forest:')
    # print(matrix(covering_forest2.matrix))

def test2_teacher():
    graph_matrix = [
        [0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 1],
        [0, 1, 1, 1, 0, 1],
    ]

    values = [0, 1, 3, 4, 5, 7]

    graph = Graph(values, graph_matrix)
    
    node = 0
    print(f'Shortest path distance from {node} to every other nodes: {graph.dijkstra(0)}')