from data_structures.relations.transitive_closure import transitive_closure
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_transitive_closure():
    print(f'{OKGREEN}RUN TEST: transitive closure.{ENDC}\n')
    
    matrix1= [
        [0, 1, 1],
        [1, 0, 0],
        [0, 0, 0]
    ]

    print(f'The matrix 1 before the transitive closure:')
    print(matrix(matrix1))

    print(f'\nThe matrix 1 after the transitive closure:')
    print(matrix(transitive_closure(matrix1)))

    print(f'\n{OKGREEN}TEST FINISHED{ENDC}\n')