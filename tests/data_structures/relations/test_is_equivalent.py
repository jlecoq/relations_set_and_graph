from data_structures.relations.is_equivalent import is_equivalent
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_is_equivalent():
    print(f'{OKGREEN}RUN TEST: equivalent relations.{ENDC}\n')
    
    matrix1 = [
        [1, 0, 1, 0, 0],
        [0, 1, 0, 1, 1],
        [1, 0, 1, 0, 0],
        [0, 1, 0, 1, 1],
        [0, 1, 0, 1, 1]
    ]

    matrix2 = [
        [1, 1, 0, 1, 1],
        [0, 1, 0, 0, 0],
        [1, 1, 1, 1, 1],
        [1, 1, 0, 1, 1],
        [0, 1, 0, 0, 1]
    ]

    print(f'The matrix 1:')
    print(matrix(matrix1))
    print(f'\nThe matrix 1 is equivalent: {is_equivalent(matrix1)}\n')

    print(f'The matrix 2:')
    print(matrix(matrix2))
    print(f'\nThe matrix 2 is equivalent: {is_equivalent(matrix2)}\n') 

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')  