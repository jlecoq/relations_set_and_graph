from data_structures.relations.is_intransitive import is_intransitive
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_is_intransitive():
    print(f'{OKGREEN}RUN TEST: intransitive relations.{ENDC}\n')
    
    matrix1 = [
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 0]
    ]

    matrix2 = [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 0]
    ]

    print(f'The matrix 1:')
    print(matrix(matrix1))
    print(f'\nThe matrix 1 is intransitive: {is_intransitive(matrix1)}\n')

    print(f'The matrix 2:')
    print(matrix(matrix2))
    print(f'\nThe matrix 2 is intransitive: {is_intransitive(matrix2)}\n') 

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')