from data_structures.relations.is_antitransitive import is_antitransitive
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_is_antitransitive():
    print(f'{OKGREEN}RUN TEST: antitransitive relations.{ENDC}\n')
    
    matrix1 = [
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 0]
    ]

    matrix2 = [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 0]
    ]

    print(f'The matrix 1:')
    print(matrix(matrix1))
    print(f'\nThe matrix 1 is antitransitive: {is_antitransitive(matrix1)}\n')

    print(f'The matrix 2:')
    print(matrix(matrix2))
    print(f'\nThe matrix 2 is antitransitive: {is_antitransitive(matrix2)}\n') 

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')