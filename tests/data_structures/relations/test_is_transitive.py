from data_structures.relations.is_transitive import is_transitive, transitive_relations, print_relations_of_transitive_matrix
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_is_transitive():
    print(f'{OKGREEN}RUN TEST: transitive relations.{ENDC}\n')
    
    # Links of the relations in the relation matrix:
    # (x, y), (y, z), (x, z)
    # (0, 1), (1, 2), (0, 2)
    # This relation is transitive.
    matrix1 = [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 0]
    ]

    matrix2 = [
        [0, 1, 0],
        [0, 0, 1], 
        [0, 0, 0]
    ]
    
    print(f'The matrix 1:')
    print(matrix(matrix1))
    print(f'\nThe matrix 1 is transitive: {is_transitive(matrix1)}\n')

    all_relations = transitive_relations(matrix1)
    print_relations_of_transitive_matrix(all_relations, 'matrix 1')

    print(f'\nThe matrix 2:')
    print(matrix(matrix2))
    print(f'\nThe matrix 2 is transitive: {is_transitive(matrix2)}\n') 

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')