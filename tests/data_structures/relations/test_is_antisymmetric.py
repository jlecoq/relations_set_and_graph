from data_structures.relations.is_antisymmetric import is_antisymmetric
from utils.print_color import OKGREEN, ENDC
from numpy import matrix

def test_is_antisymmetric():
    print(f'{OKGREEN}RUN TEST: antisymmetric relations.{ENDC}\n')

    matrix1 = [
        [1, 1, 0],
        [0, 0, 1],
        [1, 0, 1]
    ]

    matrix2 = [
        [1, 1, 1],
        [0, 0, 1],
        [1, 0, 1]
    ]

    print(f'The matrix 1:')
    print(matrix(matrix1))
    print(f'\nThe matrix 1 is antisymmetric: {is_antisymmetric(matrix1)}\n')

    print(f'The matrix 2:')
    print(matrix(matrix2))
    print(f'\nThe matrix 2 is antisymmetric: {is_antisymmetric(matrix2)}\n') 

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')