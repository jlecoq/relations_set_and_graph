from data_structures.relations_set import RelationsSet
from numpy import matrix
from math_utils.operators import *
from utils.print_color import OKGREEN, ENDC

def process(x: RelationsSet, p: RelationsSet, subset_name: str = 'p'):
    print(f'The subset {subset_name} of x is: {p}\n')

    low_bounds = x.lower_bounds(p)
    print(f'The lower bounds of {subset_name} in the set x are: {low_bounds}')

    up_bounds = x.upper_bounds(p)
    print(f'The upper bounds of {subset_name} in the set x are: {up_bounds}')

    least_upper_bound = x.supremum(p)
    print(f'The supremum of {subset_name} is: {least_upper_bound}')

    greatest_lower_bound = x.infimum(p)
    print(f'The infimum of {subset_name} is: {greatest_lower_bound}')

    min_elements = p.minimal_elements()
    print(f'The minimal elements of {subset_name} are: {min_elements}')

    max_elements = p.maximal_elements()
    print(f'The maximal elements of {subset_name} are: {max_elements}')

    l_element = p.least_element()
    print(f'The least element of {subset_name} is: {l_element}')

    l_element = p.greatest_element()
    print(f'The greatest element of {subset_name} is: {l_element}')

    universal_el = x.universal_element()
    print(f'The universal element of x is: {universal_el}')

    null_el = x.null_element()
    print(f'The null element of x is: {null_el}')

def test_relations_set():
    print(f'{OKGREEN}RUN TEST: relations set.{ENDC}\n')

    x = RelationsSet((1, 2, 3, 5, 10, 20, 30), divide_operator)
    print(f'The set x: {x}\n')

    sagittal_diagram = x.sagittal_diagram_matrix()
    print('Sagittal diagram (matrix) of the set x:')
    print(matrix(sagittal_diagram))
    print()

    print('Hasse diagram (matrix) of the set x:')
    hasse_diagram = x.hasse_diagram_matrix()
    print(matrix(hasse_diagram))
    
    print('\nSagittal diagram of x:')
    x.print_sagittal_diagram()

    print('Hasse diagram of x:')
    x.print_hasse_diagram()

    print(f'x is a lattice: {x.is_lattice()}\n')

    p_1 = RelationsSet([2, 3, 5, 10], divide_operator)
    process(x, p_1, 'p_1')

    print()
    p_2 = RelationsSet([2, 5, 10], divide_operator)
    process(x, p_2, 'p_2')

    print(f'\n{OKGREEN}TEST FINISHED{ENDC}\n')