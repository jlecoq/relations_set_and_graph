from tests.data_structures.relations.test_is_transitive import test_is_transitive
from tests.data_structures.relations.test_is_intransitive import test_is_intransitive
from tests.data_structures.relations.test_is_connex import test_is_connex
from tests.data_structures.relations.test_is_equivalent import test_is_equivalent
from tests.data_structures.relations.test_is_antisymmetric import test_is_antisymmetric
from tests.data_structures.relations.test_is_antitransitive import test_is_antitransitive
from tests.data_structures.relations.test_is_asymmetric import test_is_asymmetric
from tests.data_structures.relations.test_is_irreflexive import test_is_irreflexive
from tests.data_structures.relations.test_is_reflexive import test_is_reflexive
from tests.data_structures.relations.test_is_symmetric import test_is_symmetric
from tests.data_structures.relations.test_transitive_closure import test_transitive_closure
from tests.data_structures.test_relations_set import test_relations_set
from tests.data_structures.test_graph import test_graph

# Tests relations:
test_is_antisymmetric()
test_is_antitransitive()
test_is_asymmetric()
test_is_connex()
test_is_equivalent()
test_is_intransitive()
test_is_irreflexive()
test_is_reflexive()
test_is_symmetric()
test_is_transitive()
test_transitive_closure()

# Tests relations set:
test_relations_set()

# Tests graph:
test_graph()
